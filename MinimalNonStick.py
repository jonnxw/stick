# -*- coding: utf-8 -*-
from sage.all import *
from helper import OS

def find(order):
    """
    Bestimmt die minimalen Nicht-Stick-Graphen mit einer gegebenen Anzahl an Knoten (order). Voraussetzung: Die minimalen Nicht-Stick-Graphen mit weniger Knoten wurden schon bestimmt.
    """
    assert order > 8, "Für order <= 8 ist das trivial"
    minimals = set()

    # minimals enthält alle minimalen Nicht-Stick-Graphen mit weniger Knoten
    for n in range(8,order):
        minimals = minimals.union(OS.readGraphs("non-stick/minimal", n, mapToGraph = True))

    # Nicht-Stick-Graphen, die darauf untersucht werden sollen, ob sie minimal sind.
    nonsticks = OS.readGraphs("non-stick", order, mapToGraph=True)

    print("looking for %i minimal non-stick-graphs as induced subgraph in %i non-stick graphs of order %i" % (len(minimals), len(nonsticks), order))
    
    newminimals = set()

    # Betrachte jeden Nicht-Stick-Graphen
    for nonstick in nonsticks:
        is_minimal = True

        # Iteriere über alle minimalen Nicht-Stick-Graphen 
        for minimal in minimals:
            num_vert_nonstick = nonstick.num_verts()
            num_vert_minimal = minimal.num_verts()
            diff = num_vert_nonstick - num_vert_minimal

            # Betrachte alle induzierten Teilgraphen, die genauso viele Knoten wie der aktuell betrachtete minimale haben
            for vertices_to_delete in Combinations(range(num_vert_nonstick), diff):
                g = nonstick.copy(immutable=False)
                g.delete_vertices(vertices_to_delete)

                # Wenn sie isomorph sind, ist der aktuelle Nicht-Stick-Graph nicht minimal
                if g.is_isomorphic(minimal):
                    is_minimal = False
                    break
            if not is_minimal:
                break
        if is_minimal:
            newminimals.add(nonstick.canonical_label().sparse6_string())

    print("found %i new minimal subgraphs" % len(newminimals))
    return newminimals

