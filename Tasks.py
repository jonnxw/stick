# -*- coding: utf-8 -*-
import Stick, MinimalNonStick
from helper import OS, OrderDimensions, StickRepresentations
from sage.all import *
from multiprocessing import Pool, TimeoutError


def ComputeStickGraphsAndRepresentations(order):
    """ 
    Erzeugt und speichert alle Stick-Graphen (sparse6-Kodierungen) und zugehörigen Stickrepräsantationen (ohne Spiegelungen)
    """

    graphs_and_reps = Stick.compute_graphs_and_representations(order)

    # Extrahiere und speichere nur die Graphen
    just_graphs = set(graphs_and_reps.keys())
    OS.writeGraphs(just_graphs, "stick", order)

    # Speichere Graphen und alle zugehörigen Stickrepräsentationen
    OS.writeRepresentations(graphs_and_reps, order)

def DeriveNonStickGraphs(order):
    """
    Erzeugt alle bipartiten maximal reduzierten Graphen und leitet daraus mit Hilfe der schon erzeugten Stick-Graphen die maximal reduzierten Nicht-Stick-Graphen ab
    """
    bipartite = set(filter(lambda sparse: Stick.is_maximal_reduced(sparse),[g.copy(immutable=True).canonical_label().sparse6_string() for g in graphs.nauty_geng("%i -c -b -d2" % order)]))
    OS.writeGraphs(bipartite, "bipartite", order)
    print("Computed %i maximal reduced bipartite graphs with %i vertices" % (len(bipartite), order))

    stick = OS.readGraphs("stick", order)
    print("Loaded maximal reduced stick-graphs with %i vertices." % order)

    nonstick = bipartite.difference(stick)
    OS.writeGraphs(nonstick, "non-stick", order)
    print("Computed %i maximal reduced non-stick-graphs with %i vertices\n" % (len(nonstick), order))

def FindMinimalNonStick(order):
    """
    Bestimmt die minimalen Nicht-Stick-Graphen mit einer gegebenen Anzahl an Knoten (order). Voraussetzung: Die minimalen Nicht-Stick-Graphen mit weniger Knoten wurden schon bestimmt.
    """
    printHeading("Finding minimal non-stick-graphs with %i vertices" % order)
    minimal = MinimalNonStick.find(order)
    OS.writeGraphs(minimal, "non-stick/minimal", order)

def RemoveSymmetricRepresentations(orders = [4,5,6,7,8,9,10,11,12]):
    orders = filter(lambda n: n%2 is 0, orders)
    print(orders)
    

    for order in orders:
        print(order)
        data = OS.readRepresentations(order, mapToRepresentations=True)
        reduced = 0
        for sparse in data.keys():
            representations = data[sparse]
            #print(representations[0])
            newRepresentations = list()
            bef = len(representations)
            while(len(representations) > 0):
                rep = representations.pop()
                newRepresentations.append(rep)
                try:
                    representations.remove(StickRepresentations.reflect(rep))
                except ValueError:
                    pass
                newRepresentations.append(rep)
            data[sparse] = newRepresentations
        OS.writeRepresentations(data, order, reduced=True, mapToInt=True)

def ProcessOrderDimensions(order, graphclass = "non-stick/minimal", starttimeout = 5, maxTimeout = 20):
    def timeout_get(timeout):
        def getter(tup):
            sparse6 = tup[0]
            dimension_async = tup[1]
            dimension = None
            try:
                dimension = dimension_async.get(timeout)
            except TimeoutError:
                pass
            return (sparse6, dimension)
        return getter
    graphs = OS.readGraphs(graphclass, order)
    found = dict()
    try:
        found = OS.read(graphclass + "/dimensions", order)
    except Exception:
        pass
    graphs = graphs.difference(set(found.keys()))

    print("Already have dimensions of %i graphs, looking for dimensions on %i graphs" % (len(found), len(graphs)))
    timeout = starttimeout
    while len(graphs) > 0 and timeout <= maxTimeout:
        print("Looking at %i graphs. timeout: %is" % (len(graphs), timeout))
        p = Pool(4)
        async_res = [ (sparse6, p.apply_async( OrderDimensions.get_dimension, (sparse6,) )) for sparse6 in graphs]
        results = map(timeout_get(timeout), async_res)
        p.terminate()
        p.join()  
        foundTemp = filter(lambda res: res[1] is not None, results)
        for i in range(len(foundTemp)):
            found[foundTemp[i][0]] = foundTemp[i][1] 
        print("found %i new dimensions. Now have %i dimensions" % (len(foundTemp), len(found)))
        OS.write(found, graphclass + "/dimensions", order)
        graphs = [res[0] for res in results if res[1] is None]
        timeout = timeout*2


####################################
# Helpers for printing information #
####################################
def printBanner(text):
    size = len(text)
    print("\n"+"#"*(size+6))
    print("#" + " "*(size+4) + "#")
    print("#  " + text + "  #")
    print("#" + " "*(size+4) + "#")
    print("#"*(size+6))

def printHeading(text):
    size = len(text)
    print("\n"+"#"*(size+4))
    print("# " + text + " #")
    print("#"*(size+4))