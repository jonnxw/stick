import sage.all
from sage.all import Graph

def simple_graph_from_sparse6(s):
    """ Creates a simple graph (no multiedges) from a sparse6 string """
    assert type(s) is str, "Input must be a string"

    temp = Graph(s)
    n = temp.num_verts()
    gr = Graph(n)
    gr.allow_multiple_edges(False)
    for edges in temp.edges():
        gr.add_edge(edges[0], edges[1])
    return gr